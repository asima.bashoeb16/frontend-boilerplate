import React from 'react';
import { shallow} from 'enzyme';
import Profile from './profile';
import ProfileNavigation from './profileNavigation';

describe('Profile Component', () => {
  it('renders profile component correctly', () => {
    const wrapper = shallow(
      <Profile />
    );
  });
  it('should not have  input field username', ()=> {
    //should not have user input field
    expect(shallow(<Profile/>).find('input')).toHaveLength(0);
  });
  it('should render profile navigation bar', () => {
    expect(shallow(<ProfileNavigation/>));
  });
});