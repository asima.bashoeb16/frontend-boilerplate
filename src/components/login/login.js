import React from 'react';
import { Redirect } from 'react-router';
import HTTPService from '../../shared/httpService';
import Notification from '../notification/notification';
import './login.css';

class Login extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      redirect: false,
      alertm: 'danger'
    };
    this.formObj = {
      username:'',
      password:''
    };
    this.banner = false;
    this.submitDisabled = true;
    this.httpServ = new HTTPService();
    this.verifyLogin = this.verifyLogin.bind(this);
    this.enableSubmit = this.enableSubmit.bind(this);
    if(localStorage.getItem('access_token_login')!== null && localStorage.getItem('access_token_login')!== undefined) {
      this.state.redirect = true;
    }
  }

  // Method called when filling the username and password fields
  handleUserInput = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    if(name === 'username') {
      this.formObj.username = value;
    } else if(name === 'password') {
      this.formObj.password = value;
    }
    this.enableSubmit();
  }

  // Enabling the Login button only after both username and password fields are filled
  enableSubmit() {
    if(this.formObj.username !== '' && this.formObj.password !== '') {
      this.submitDisabled = false;
    } else {
      this.submitDisabled = true;
    }
    this.forceUpdate();
  }
  
  // API Call for login
  handleLogin(event) {
    this.requestBody = {
      username: this.formObj.username,
      password: this.formObj.password
    };
    this.httpServ.postLogin('/login', this.requestBody)
    .then(response => { 
      const statusCode = response.status;
      const data = response.json(); 
      return Promise.all([statusCode, data]);
    }) 
    .then(res => { 
      return {
        statusCode:res[0], 
        data:res[1]
      }; 
    }) 
    .then(json => {
      const status = json.statusCode;
      const data = json.data;
      if(status === 200) {
        localStorage.setItem('access_token_login', data.token);
        this.verifyLogin();       
      } else {
        this.showErrorNotificationBanner(data.err);
      }
    });
  }

  // Handling the Error response from the backend
  showErrorNotificationBanner(errorMessage) {
    this.message = errorMessage;
    this.banner = true;
    this.forceUpdate();
  }

  // API Call to verify login using the access token from login Api call
  verifyLogin() {
    return this.httpServ.getVerifyLogin('/login/verify')
    .then((response) => {
      const statusCode = response.status;
      const data = response.json();
      return Promise.all([statusCode, data]);
    })
    .then((result) => {
      var status=result[0];
      var data=result[1];
      if(status === 200) {
        this.setState({redirect:true});
      } else {
        alert (data);
      }         
    })
    .catch(error => console.log(error)); 
  }

  render() {
    if(this.state.redirect) {
      return (<Redirect to={{
        pathname: '/profile'
      }} />);
    }
    return (
      <div className='LoginPage'>
        <div className='wrapper Login'>
          <div id='formContent'>
            {this.banner && <Notification status={this.state.alertm} message={this.message}/>}
            <form>
              <div className='formItems'>
                <h3 className='text-center loginClass'>Login</h3>
                <label className='labelClass'>Username</label>
                <div className='row'>
                  <div className='col'>
                    <input type='text' autoComplete='off' name='username' className='form-control rectangle form-control-lg bform' value={this.formObj.username} required onChange={this.handleUserInput} id='username'/>
                  </div>
                </div>  
                <label className='password'>Password</label>
                <div className='row'>
                  <div className='col'>
                    <input name='password' type='password' value={this.formObj.password} onChange={this.handleUserInput} required className='form-control rectangle form-control-lg bform' id='password'/>
                  </div>
                </div>  
                <div>
                  <small id='emailHelp'>
                    <a href='/#' className='form-text text-muted text-right'>Forgot Password?</a>
                  </small>
                </div>
                <div className='login-button'>             
                  <button type='button' className='btn btn-warning btn-lg btn-block' disabled={this.submitDisabled} onClick={this.handleLogin.bind(this)}>Login</button>
                  <div className='trouble'><a id='emailHelp' className='form-text text-muted text-center accountText' href='/#'>No account yet?</a></div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>  
    );
  }
}
export default Login;