class HTTPService {
  constructor() {
    this.baseUrl = 'http://localhost:3000';
    this.appHeaders = {
      'Content-Type': 'application/json'
    };
  }

  getToken() {
    return localStorage.getItem('access_token_login');
  }

  postLogin(url, reqBody) {
    const finalURL = this.baseUrl + url;
    return fetch(finalURL, {
      method: 'POST',
      headers: this.appHeaders,
      body: JSON.stringify(reqBody)
    })
    .then(response => response);
  }

  getVerifyLogin(url) {
    const finalURL = this.baseUrl + url;
    const appHeader = this.appHeaders;
    appHeader.Authorization = `Bearer ${this.getToken()}`;
    return fetch(finalURL, {
      method: 'GET',
      headers: appHeader
    })
    .then(response => response);
  }

  getProfileData(url) {
    const finalURL = this.baseUrl + url;
    const appHeader = this.appHeaders;
    appHeader.Authorization = `Bearer ${this.getToken()}`;
    return fetch(finalURL, {
      method: 'GET',
      headers: appHeader
    })
    .then(response => response);
  }
}
export default HTTPService;