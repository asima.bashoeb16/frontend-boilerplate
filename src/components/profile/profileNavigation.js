import React from 'react';
import { Redirect } from 'react-router';

class ProfileNavigation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      redirect: false
    };
    this.logout = this.logout.bind(this);  
  }

  // This method is called on click of logout, which removes the access token and redirects the user to login page
  logout() {
    setTimeout(function() {
      localStorage.removeItem('access_token_login');
      this.forceUpdate();
    }.bind(this), 1);
    this.setState({redirect:true});
  }

  render() {
    if(this.state.redirect){
      return (<Redirect to={{
        pathname: '/login'
      }} />);
    }
        
    return ( 
      <div className='navigation'>
        <nav className='navbar navbar-expand-lg navbar-dark bg-dark'>
          <a className='navbar-brand' href='/profile'>FRONTMEN</a>
          <button className='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarSupportedContent' aria-controls='navbarSupportedContent' aria-expanded='false' aria-label='Toggle navigation'>
            <span className='navbar-toggler-icon'></span>
          </button>
          <div className='collapse navbar-collapse' id='navbarSupportedContent'>
            <ul className='navbar-nav mr-auto'>
              <li className='nav-item active'>
                <a className='nav-link' href='/profile'>MY PROFILE</a>
              </li>
              <li className='nav-item'>
                <a className='nav-link' href='/profile'>GROUPS</a>
              </li>
              <li className='nav-item'>
                <a className='nav-link' href='/#'>PEOPLE</a>
              </li>
            </ul> 
            <form className='form-inline my-2 my-lg-0'>
              <input className='form-control mr-sm-2' type='search' placeholder='Search' aria-label='Search'/>
              <button className='btn btn-outline-success my-2 my-sm-0' type='submit'>Search</button>
            </form>
            <ul className='navbar-nav my-lg-0'>
              <li className='nav-item'>
                <a className='nav-link' href='/#' onClick={this.logout}>LOGOUT</a>
              </li>
            </ul> 
          </div>
        </nav>
      </div>
    );
  }
}
export default ProfileNavigation;