import React from 'react';
import { shallow } from 'enzyme';
import Login from './login';

describe('Login component tests', ()=> {
  const wrapper = shallow(<Login />);

  it('should have a button component', ()=> {
    //There should be only one button
    expect(wrapper.find('button')).toHaveLength(1);

    //Button should be of type button
    expect(wrapper.find('button')
      .type())
      .toEqual('button');
  
    //Button should have matching text
    expect(wrapper.find('button').text()).toEqual('Login');
  });

  it('should have input field for username', ()=> {
    //Username input field should be present
    expect(wrapper.find('input#username')).toHaveLength(1);
  });

  it('should have password field', () => {
    //Password input field should be present
    expect(wrapper.find('input#password')).toHaveLength(1);
  });
});