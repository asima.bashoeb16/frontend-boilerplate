import React from 'react';
import { Redirect } from 'react-router';
import HTTPService from '../../shared/httpService';
import ProfileNavigation from './profileNavigation';
import './profile.css';

class Profile extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      firstname: '',
      lastname: '',
      email: '',
      username: '',
      image: 'https://homepages.cae.wisc.edu/~ece533/images/watch.png'
    };
    this.logout = false;
    this.httpServ = new HTTPService();
    if(localStorage.getItem('access_token_login')!== null && localStorage.getItem('access_token_login')!== undefined) {
      this.getProfile();
    } else {
      this.logout = true;
    }
  }

  // API Call to get the profile data from the backend
  getProfile() {
    return this.httpServ.getProfileData('/api/v1/users/myprofile')
    .then((response) => {
      const statusCode = response.status;
      const data = response.json();
      return Promise.all([statusCode, data]);
    })
    .then((result) => {
      var status=result[0];
      var data=result[1];
      if(status === 200) {
        this.setProfile(data);
      } else {
        alert (data);
      }         
    })
    .catch(error => console.log(error)); 
  }

  // Displaying the data on profile page
  setProfile(data) {
    this.setState({
      firstname: data.user.firstname,
      lastname: data.user.lastname,
      email: data.user.email,
      username: data.user.username
    });
    this.forceUpdate();
  }

  render() {
    if(this.logout){
      return (<Redirect to={{
        pathname: '/login'
      }} />);
    }
    return ( 
      <div className='profile'>
        <ProfileNavigation/>
        <div className='container emp-profile'>
          <form method='post'>
            <div className='row'>
              <div className='col-md-4'>
                <div className='profile-img'>
                  <img src={this.state.image} alt=''/>
                  <div className='file btn btn-lg btn-primary'>
                    Change Photo
                    <input  className= 'test' type='file' name='file' id='file'/>
                  </div>
                </div>
              </div>
              <div className='col-md-6'>
                <div className='profile-head'>
                  <h5> {this.state.firstname} {this.state.lastname} </h5>
                  <h6> Frontend Developer </h6>
                  <p className='proile-branch'>BRANCH <span>{this.state.lastname}</span></p>
                  <ul className='nav nav-tabs' id='myTab' role='tablist'>
                    <li className='nav-item'>
                      <a className='nav-link active' id='home-tab' data-toggle='tab' href='#home' role='tab' aria-controls='home' aria-selected='true'>About</a>
                    </li>
                  </ul>
                </div>
              </div>
              <div className='col-md-2'>
                <input type='submit' className='profile-edit-btn' name='btnAddMore' value='Edit Profile'/>
              </div>
            </div>
            <div className='row'>
              <div className='col-md-4'>
                <div className='profile-work'>
                  <p>WORK LINK</p>
                  <a href='www.linkedin.com'>Linkedin</a><br/>
                  <a href='www.github.com'>Github</a><br/>
                  <p>SKILLS</p>
                  <div>Angular</div><br/>
                  <div>React</div><br/>
                  <div>HTML</div><br/>
                  <div>Javascript</div><br/>
                  <div>CSS</div><br/>
                </div>
              </div>
              <div className='col-md-8'>
                <div className='tab-content profile-tab' id='myTabContent'>
                  <div className='tab-pane fade show active' id='home' role='tabpanel' aria-labelledby='home-tab'>
                    <div className='row'>
                      <div className='col-md-6'>
                        <label>Username</label>
                      </div>
                      <div className='col-md-6'>
                        <p>{this.state.username}</p>
                      </div>
                    </div>
                    <div className='row'>
                      <div className='col-md-6'>
                        <label>First Name</label>
                      </div>
                      <div className='col-md-6'>
                        <p>{this.state.firstname}</p>
                      </div>
                    </div>
                    <div className='row'>
                      <div className='col-md-6'>
                        <label>Last Name</label>
                      </div>
                      <div className='col-md-6'>
                        <p>{this.state.lastname}</p>
                      </div>
                    </div>
                    <div className='row'>
                      <div className='col-md-6'>
                        <label>Email</label>
                      </div>
                      <div className='col-md-6'>
                        <p>{this.state.email}</p>
                      </div>
                    </div>
                    <div className='row'>
                      <div className='col-md-6'>
                        <label>Frontmen Start Date</label>
                      </div>
                      <div className='col-md-6'>
                        <p>01-01-2019</p>
                      </div>
                    </div>
                    <div className='row'>
                      <div className='col-md-6'>
                        <label>Spoken Languages</label>
                      </div>
                      <div className='col-md-6'>
                        <p>English, Dutch</p>
                      </div>
                    </div>
                    <div className='row'>
                      <div className='col-md-6'>
                        <label>Mobile phone</label>
                      </div>
                      <div className='col-md-6'>
                        <p>123 546 677</p>
                      </div>
                    </div>
                    <div className='row'>
                      <div className='col-md-6'>
                        <label>Office phone</label>
                      </div>
                      <div className='col-md-6'>
                        <p>123 456 7890</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>           
        </div>
      </div>
    );
  }
}
export default Profile;