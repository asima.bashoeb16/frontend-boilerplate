import React from 'react';

class Notification extends React.Component {
  render() {
    const divStyle = {
      zIndex: 999,
      textAlign: 'center'
    };

    let className = 'alert alert-secondary';
    if (this.props.status === 'danger') {
      className += 'alert alert-danger';
    } else if (this.props.status === 'success') {
      className += 'alert alert-success';
    }

    return (
      <div className={className} role='alert' style={divStyle}>
        {this.props.message}
      </div>
    );
  }
}
export default Notification;