# Frontmen React App

## Description

This is a boilerplate application for User Login and User Profile. It is built using React to create and import components, make api calls, authorise the user to Log in to his Profile.

## Prerequisites

- Node.js (12.16.3) and NPM (6.14.4)
- Git
- Visual Studio Code or any other IDE

## Installation/Demo

- 'npm install' to install all dependencies

- 'npm run test' to run tests

- 'npm run build' to build your application

- 'npm run lint' to check for Linting errors

- 'npm start' to start your application on 'http://localhost:4000'. Upon successlful Logging, the authorised user will be redirected to his Profile.

## Technologies and Libraries Used

- React - 16.13.1
- Bootstrap - 4.4.1 
- Jest - 24.9.0 and Enzyme - 3.11.0 
- Eslint - 6.8.0 

## Browser Compatability
- Google Chrome  81.x/latest
- Mozilla Firefox 75.x/latest
- Microsoft Edge 44.x/latest
- Internet Explorer 11
